import axios from 'axios';
import { Message, Loading } from 'element-ui';
import router from '../../router';


axios.defaults.timeout = 10000;
axios.interceptors.request.use(config => {
  showFullScreenLoading();
  return config;
}, (err) => {
  tryHideFullScreenLoading();
  Message.error("Unknown error. Please try again.");
  return Promise.resolve(err);
});

axios.interceptors.response.use(response => {
  tryHideFullScreenLoading();
	
  const data = response.data;
  switch (data.status) {
    case "401":
      sessionStorage.removeItem('staff_id');
      sessionStorage.removeItem('cs_log_id');
      Message.error(data.message);
			
      router.push({ 'path': '/' });
      break;
    case "400":
    case "500":
      if (data.message !== null) {
        Message.error(data.message);
      } else {
        Message.error("Unknown error. Please try again.");
      }
      break;
    default:
      return data;
  }
    
  return data;
}, (err) => { 
  tryHideFullScreenLoading();
  if(err.message.indexOf('timeout')!=-1) Message.error("Request timeout. Please try again.");
  return Promise.reject(err);
});


let loading 
function startLoading() {
  loading = Loading.service({lock: true});
}
function endLoading() {   
  loading.close()
}

let needLoadingRequestCount = 0
export function showFullScreenLoading() {
  if (needLoadingRequestCount === 0) {
    startLoading()
  }
  needLoadingRequestCount++;
}

export function tryHideFullScreenLoading() {
  if (needLoadingRequestCount <= 0) return
    needLoadingRequestCount--
  if (needLoadingRequestCount === 0) {
    endLoading();
  }
}

export default axios;