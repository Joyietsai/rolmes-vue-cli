export const searchMixin = {
  methods: {
    filterText() {
      const rows = this.$refs.tableRef.$refs.bodyWrapper.getElementsByClassName(
        'el-table__row'
      )
      for (let row of rows) {
        let cells = row.getElementsByTagName('td')
        for (let cell of cells) {
          let innerText = cell.innerText.toLowerCase()
          let search = this.search.toLowerCase()
          if (innerText.indexOf(search) > -1 ) {
            row.style.display = ''
            break
          } else {
            row.style.display = 'none'
          }
        }
      }
    },
    resetQuery() {
      const rows = this.$refs.tableRef.$refs.bodyWrapper.getElementsByClassName(
        'el-table__row'
      )
      this.search = ''
      for (let row of rows) {
        row.style.display = ''
      }
    }
  }
}
