
import router from '../../router';

export default{
  install(Vue,options)
  {
    Vue.prototype.checkAuth = function () { 
	  if(!sessionStorage.getItem('staff_id')) {
	    this.$message.error( this.$t('Message.NoAuth') );
	    router.push({ 'path': '/' });
	  }
    }
  }
}