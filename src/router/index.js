import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/Home'
import ForgotPW from '@/components/ForgotPW'
import ResetPW from '@/components/ResetPW'
import Settings from '@/components/Settings'
import KB from '@/components/KB'

import Inbox from '@/components/Inbox'
import Log from '@/components/Log'

import BasicInfo from '@/components/logs/BasicInfo'
import Contact from '@/components/logs/Contact'
import EntranceLog from '@/components/logs/EntranceLog'
import EntranceHistory from '@/components/logs/EntranceHistory'
import Evidence from '@/components/logs/Evidence'
import EvidenceCategory from '@/components/logs/EvidenceCategory'
import Personnel from '@/components/logs/Personnel'
import PhotoVideo from '@/components/logs/PhotoVideo'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/forgot-pw',
      name: 'ForgotPW',
      component: ForgotPW
    },
    {
      path: '/reset-pw/:token',
      name: 'ResetPW',
      component: ResetPW
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings
    },
    {
      path: '/kb',
      name: 'KB',
      component: KB
    },
    {
      path: '/inbox',
      name: 'Inbox',
      component: Inbox
    },
    {
      path: '/log',
      name: 'Log',
      component: Log,
      children: [
        { path: ':cs_log_no', name: 'BasicInfo', component: BasicInfo },
        { path: '/contact/:cs_log_no', name: 'Contact', component: Contact },
        { path: '/entrance/:cs_log_no', name: 'EntranceLog', component: EntranceLog },
        { path: '/entrance-history/:cs_log_no', name: 'EntranceHistory', component: EntranceHistory },
        { path: '/evidence/:cs_log_no', name: 'Evidence', component: Evidence },
        { path: '/evidence-category/:cs_log_no', name: 'EvidenceCategory', component: EvidenceCategory },
        { path: '/personnel/:cs_log_no', name: 'Personnel', component: Personnel },
        { path: '/photo-video/:cs_log_no', name: 'PhotoVideo', component: PhotoVideo },
      ]
    },
  ]
})
