// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'url-search-params-polyfill'; 
import Vue from 'vue'
import ElementUI from 'element-ui';
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'
import App from './App'
import router from './router'
import axios from './components/mixins/request.js';
import VueAxios from 'vue-axios'
import { Message } from 'element-ui';

import NavHeader from './components/parts/_NavHeader'
import SubHeader from './components/parts/_SubHeader'
import SideMenu from './components/parts/_SideMenu'

import vSelect from 'vue-select'
import ToggleButton from 'vue-js-toggle-button'
import { CONFIG as SiteConfig } from '../config/config.js'
import VueI18n from 'vue-i18n'
import utility from './components/mixins/utility'

Vue.config.productionTip = false;
Vue.component('nav-header', NavHeader);
Vue.component('sub-header', SubHeader);
Vue.component('side-menu', SideMenu);
Vue.component('v-select', vSelect);

Vue.use(ToggleButton);
Vue.use(ElementUI);
Vue.use(VueI18n)
Vue.use(utility);

locale.use(lang)

Vue.prototype.axios = axios;
Object.defineProperty(Vue.prototype, '$conf', { value: SiteConfig });

const i18n = new VueI18n({
  locale: 'en', 
  messages: {
    'en-US': require('./assets/lang/en-US'),
    'zh-TW': require('./assets/lang/zh-TW'),
    'zh-CN': require('./assets/lang/zh-CN')
  },
  silentTranslationWarn: true
})

//Filter
Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

new Vue({
  el: '#app',
  render: h => h(App),
  router,
  i18n,
  axios,
  components: { App },
  template: '<App/>',
})
