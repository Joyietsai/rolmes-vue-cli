const CONFIG = {

	//API_URL: 'http://localhost:45304/api',
	API_URL: 'http://10.20.1.120:1117/api',

	PHOTO_URL: '',

	//[0]Item#, [1]Quantity, [2]Category, [3]Sub Category, [4]Is Biohazard, [5]Make, [6]Caliber, [7]Color, [8]Year, [9]Model, [10]Serial#, 
	//[11]Type, [12]Length, [13]Body, [14]VIN#, [15]Registration#, [16]State, [17]Mileage, [18]Manufactured Date, [19]Plate#, [20]Bill Amount, 
	//[21]Coin Amount, [22]Total, [23]Recovered Date, [24]Recovered Time, [25]Where Recovered, [26]Recovered By, [27]Evidence Marking, [28]Lab#, [29]Brief Description

	EVID_DE: [true, true, true, true, true, true, false, false, false, false, false,
			  false, false, false, false, false, false, false, false, false, false,
			  false, false, true, true, true, true, true, true, true],
			  
	EVID_WE: [true, true, true, true, true, true, true, false, false, true, true,
			  true, true, false, false, false, false, false, false, false, false,
			  false, false, true, true, true, true, true, true, true],
			  
	EVID_VE: [true, true, true, true, true, true, false, true, true, true, false,
			  false, false, true, true, true, true, true, true, true, false,
			  false, false, true, true, true, true, true, true, true],
			  
	EVID_CU: [true, true, true, true, true, false, false, false, false, false, false,
			  false, false, false, false, false, false, false, false, false, true,
			  true, true, true, true, true, true, true, true, true]
		  
}

export { CONFIG };